------------------------------------------------------
-------- ASPdatabase.NET Installation Readme  --------
------------------------------------------------------

This zip package is built to be installed 
using the Microsoft Web Platform Installer.  

The AppRoot folder of this zip file can be 
setup in IIS without using the MS WPI, 
though the WPI simplifies the installation process.  

The WPI will configure an app database and a 
samples database in SQL Server Express; 
and it will configure the web app to run under IIS.

------------------------------------------------------

Download Microsoft WPI: 
http://www.microsoft.com/web/downloads/platform.aspx

------------------------------------------------------

For help installing without using WPI, see this page:
www.aspdatabase.net/instructions

------------------------------------------------------

Copyright � 2014 ASPdatabase.NET LLC 